FROM node:lts-alpine3.9

RUN mkdir /app
WORKDIR /app

COPY package.json /app 

RUN yarn install

COPY . /app

RUN yarn build

EXPOSE 3000

CMD yarn start